import React, { useState } from "react";
import Counter from "./components/counter";

function App() {
  const [count, setCount] = useState(0);

  const increment = () => {
    setCount(count + 1);
  };

  const decrement = () => {
    setCount(count - 1);
  };

  return (
    <Counter />
  );
}

export default App;
