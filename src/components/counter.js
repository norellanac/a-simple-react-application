 import React, { useState } from "react";

 function Counter() {
   const [count, setCount] = useState(0);

   const increment = () => {
     setCount(count + 1);
   };

   const decrement = () => {
     setCount(count - 1);
   };

   return (
     <div>
       <h1>Simple React App</h1>
       <p>Count: {count}</p>
       <p>{count > 3? 'Counter is getting higer': null}</p>
       <button className="btn-success mx-auto" onClick={increment}>Increment</button>
       <button className="btn-danger mx-auto" onClick={decrement}>Decrement</button>
     </div>
   );
 }

 export default Counter;
